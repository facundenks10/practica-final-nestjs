import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSourceConfig } from './config/data.source';
import { DevtoolsModule } from '@nestjs/devtools-integration';
import { UsersModule } from './modules/users/users.module';
import { WalletModule } from './modules/wallet/wallet.module';
import { TransactionModule } from './modules/transaction/transaction.module';
import { OrdersModule } from './modules/orders/orders.module';
import { ShoppingSessionModule } from './modules/shopping_session/shopping_session.module';
import { CartItemsModule } from './modules/cart_items/cart_items.module';
import { ProductsModule } from './modules/products/products.module';
import { OrderItemsModule } from './modules/order_items/order_items.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [ConfigModule.forRoot({
    envFilePath: `.${process.env.NODE_ENV}.env`,
    isGlobal: true,
  }),
  DevtoolsModule.register({
    http: process.env.NODE_ENV !== 'production',
    port:8001
  }),
  TypeOrmModule.forRoot({ ...DataSourceConfig}),
  UsersModule,
  WalletModule,
  TransactionModule,
  OrdersModule,
  ShoppingSessionModule,
  CartItemsModule,
  ProductsModule,
  OrderItemsModule,
  AuthModule
  ]
})
export class AppModule {}
