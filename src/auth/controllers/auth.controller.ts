import { Body, Controller, Post, UnauthorizedException, UseGuards } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { AuthDTO } from '../dto/auth.dto';
import { ApiTags } from '@nestjs/swagger';
import { PublicAccess } from '../decorators/public.decorator';
import { AuthGuard } from '../guards/auth.guard';


@ApiTags('Auth')
@UseGuards(AuthGuard)
@Controller('auth')
export class AuthController {
    constructor(
      private readonly authService: AuthService,
    ){}
    
    @PublicAccess()
    @Post('login')
    async login(@Body() { email, password }: AuthDTO
    ) {
        const userValidate = await this.authService.validateUser(
          email,
          password,
        );

        if (!userValidate) {
          throw new UnauthorizedException('Data not valid');
        }
        
        const jwt = await this.authService.generateJWT(userValidate);

        return jwt;
      }
}