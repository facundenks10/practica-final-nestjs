import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { PUBLIC_KEY, ROLES_KEY } from '../../constants/key-decorators';
import { ROLES } from '../../constants/roles';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector
  ) { }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    const isPublic = this.reflector.get<boolean>(
      PUBLIC_KEY,
      context.getHandler(),
    )

    if (isPublic) {
      return true;
    }

    const roles = this.reflector.get<Array<keyof typeof ROLES>>(
      ROLES_KEY,
      context.getHandler(),
    )

    // --- Aqui trameos el valor del contexto cuando se usa admin guard ---
    /* const admin = this.reflector.get<string>(
      ADMIN_KEY,
      context.getHandler(),
    ) */

    const req = context.switchToHttp().getRequest<Request>();

    const { roleUser } = req;

    /* -----Permisos para que el admin que use el gard ADMIN----- */
    /* if(roles === undefined){
      if(!admin){
        return true;   
      }else if(admin && roleUser === admin){
        return true;
      }else{
        throw new UnauthorizedException('No tienes permisos para esta operacion');
      }
    } */

    // ----- Permisos para que el admin tenga acceso aunque sea rol CLIENT -----

    if (roleUser === ROLES.ADMIN) {
      return true;
    }


    const isAuth = roles.some(role => role === roleUser);

    //Si no encuentra el rol de usuario no es compatible con lo que viene en el contexto no esta autorizado
    if (!isAuth) {
      throw new UnauthorizedException('No tienes permisos para esta operacion con tu rol actual: ' + roleUser);
    }

    return true;
  }
}
