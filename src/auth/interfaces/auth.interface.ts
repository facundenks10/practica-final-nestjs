import { ROLES } from "../../constants/roles";
import { UsersEntity } from "../../modules/users/entities/users.entity";


export interface PayloadToken{
    sub: string;
    lastName: string;
    firstName: string;
    picture: string;
    levelAuthority: ROLES;
    
}

export interface AuthResponse {
    accessToken: string;
    user: UsersEntity;
  }

export interface AuthBody{
    email: string;
    password: string;
}

export interface AuthTokenResult {
    sub: string;
    lastName: string;
    firstName: string;
    picture: string;
    levelAuthority: string;
    iat:  number;
    exp:  number;
}

export interface IUseToken {
    sub: string;
    lastName: string;
    firstName: string;
    picture: string;
    levelAuthority: string;
    isExpired: boolean;
}