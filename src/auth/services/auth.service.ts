import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { AuthResponse, PayloadToken } from '../interfaces/auth.interface';
import { UsersEntity } from '../../modules/users/entities/users.entity';
import { UsersService } from '../../modules/users/services/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService
  ) { }
  public async validateUser(email: string, password: string): Promise<UsersEntity | null> {
    try {
      const userByEmial = await this.userService.findBy({
        key: 'email',
        value: email,
      });

      if (userByEmial) {
        const match = await bcrypt.compare(password, userByEmial.password);
        if (match) {
          return userByEmial;
        } else {
          throw new BadRequestException('El password no es correcto.');
        };
      } else {
        throw new NotFoundException('El usuario no existe.');
      }
    } catch (error) {
      if (error instanceof BadRequestException || error instanceof NotFoundException) {
        throw error;
      } else {
        throw new ConflictException('Hubo un error durante el proceso.');
      }
    }

  }

  public signJWT({
    payload,
    secret,
    expires,
  }: {
    payload: jwt.JwtPayload;
    secret: string;
    expires: number | string;
  }): string {
    return jwt.sign(payload, secret, { expiresIn: expires });
  }

  public async generateJWT(user: UsersEntity): Promise<AuthResponse> {

    const getUser = await this.userService.findUserById(user.id);

    if (getUser) {
      user = getUser;
    }

    const payload: PayloadToken = {
      sub: getUser.id,
      firstName: getUser.firstName,
      lastName: getUser.lastName,
      picture: getUser.picture,
      levelAuthority: getUser.level_authority,
    };

    return {
      accessToken: this.signJWT({
        payload,
        secret: process.env.JWT_SECRET,
        expires: '24h',
      }),
      user,
    };
  }
}
