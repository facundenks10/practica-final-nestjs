import { Global, Module } from '@nestjs/common';
import { CommonService } from './common.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsEntity } from 'src/modules/products/entities/products.entity';
import { WalletEntity } from 'src/modules/wallet/entities/wallet.entity';
import { CartItemsEntity } from 'src/modules/cart_items/entities/cart_items.entity';
import { UsersEntity } from 'src/modules/users/entities/users.entity';
import { ShoppingSessionEntity } from 'src/modules/shopping_session/entities/shopping_session.entity';
import { OrderEntity } from 'src/modules/orders/entities/order.entity';
import { OrderItemsEntity } from 'src/modules/order_items/entities/order_items.entity';

@Global()
@Module({
  providers: [CommonService],
  imports: [
    TypeOrmModule.forFeature([ProductsEntity, WalletEntity, CartItemsEntity, UsersEntity, ShoppingSessionEntity, OrderEntity, OrderItemsEntity]),
  ],
  exports: [CommonService],

})
export class CommonModule {}