import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CartItemsBaseDTO, CartItemsDTO } from 'src/modules/cart_items/dto/cart_items.dto';
import { CartItemsEntity } from 'src/modules/cart_items/entities/cart_items.entity';
import { OrderItemsDTO } from 'src/modules/order_items/dto/order_items.dto';
import { OrderItemsEntity } from 'src/modules/order_items/entities/order_items.entity';
import { ProductsEntity } from 'src/modules/products/entities/products.entity';
import { ShoppingSessionEntity } from 'src/modules/shopping_session/entities/shopping_session.entity';
import { UsersEntity } from 'src/modules/users/entities/users.entity';
import { WalletDTO } from 'src/modules/wallet/dto/wallet.dto';
import { WalletEntity } from 'src/modules/wallet/entities/wallet.entity';
import { ErrorManager } from 'src/utils/error.manager';
import { userToken } from 'src/utils/user.token';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class CommonService {

    constructor(
        @InjectRepository(ShoppingSessionEntity)
        private readonly shoppingSessionRepository: Repository<ShoppingSessionEntity>,
        @InjectRepository(ProductsEntity)
        private readonly productRepository: Repository<ProductsEntity>,
        @InjectRepository(WalletEntity)
        private readonly walletRepository: Repository<WalletEntity>,
        @InjectRepository(CartItemsEntity)
        private readonly cartItemsRepository: Repository<CartItemsEntity>,
        @InjectRepository(UsersEntity)
        private readonly userRepository: Repository<UsersEntity>,
        @InjectRepository(OrderItemsEntity)
        private readonly orderItemsRepository: Repository<OrderItemsEntity>,
    ) { }

    // User Repository
    public async findUserById(id: string): Promise<UsersEntity> {
        try {
            const user: UsersEntity = await this.userRepository
                .createQueryBuilder('user')
                .where({ id })
                .leftJoinAndSelect('user.wallet', 'wallet')
                .leftJoinAndSelect('user.orders', 'order')
                .leftJoinAndSelect('user.transactionIncludes', 'transaction')
                .leftJoinAndSelect('user.shopping_session', 'shopping_session')
                .getOne();
            if (!user) {
                throw new NotFoundException('No se encontro usuario.');
            }
            return user;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //ShoppingSession Repository

    public async findShoppingSessionById(id: string): Promise<ShoppingSessionEntity> {
        try {
            const shoppingSession: ShoppingSessionEntity = await this.shoppingSessionRepository
                .createQueryBuilder('shopping_session')
                .where({ id })
                .getOne();
            if (!shoppingSession) {
                throw new NotFoundException('No se encontro carrito de compras.');
            }
            return shoppingSession;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findShoppingSessionByUserId(id: string): Promise<ShoppingSessionEntity> {
        try {
            const shoppingSession: ShoppingSessionEntity = await this.shoppingSessionRepository.findOneBy({ user: { id: id } });
            if (!shoppingSession) {
                throw new NotFoundException('No se encontro carrito de compras.');
            }
            return shoppingSession;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async deleteShoppingSessionById(id: string): Promise<DeleteResult> {
        try {
            const shoppingSession: DeleteResult = await this.shoppingSessionRepository.delete(id);
            if (shoppingSession.affected === 0) {
                throw new NotFoundException('No se encontro carrito de compras.');
            }
            return shoppingSession;
        }
        catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //Products Repository

    public async findProductById(id: string): Promise<ProductsEntity> {
        try {
            const product: ProductsEntity = await this.productRepository
                .createQueryBuilder('product')
                .where({ id })
                .leftJoinAndSelect('product.cart_items', 'cart_item')
                .leftJoinAndSelect('product.order_items', 'order_item')
                .getOne();
            if (!product) {
                throw new NotFoundException('No se encontro Producto.');
            }
            return product;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async updateStockProduct(id: string, stock: number): Promise<ProductsEntity> {
        try {
            const product = await this.productRepository.findOneBy({ id: id });
            if (!product || !product.activated) {
                throw new NotFoundException('No se encontro Producto.');
            }

            let newStock = product.stock - stock;

            if (newStock < 0) {
                throw new BadRequestException('No hay stock suficiente.');
            }

            product.stock = newStock;
            await this.productRepository.update({ id: id }, { stock: newStock });

            return product;
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async updateStockAddProduct(id: string, stock: number): Promise<ProductsEntity> {
        try {
            const product = await this.productRepository.findOneBy({ id: id });
            if (!product || !product.activated) {
                throw new NotFoundException('No hay stock suficiente');
            }

            let newStock = product.stock + stock;

            if (newStock < 0) {
                throw new NotFoundException('No hay stock suficiente');
            }

            product.stock = newStock;
            await this.productRepository.update({ id: id }, { stock: newStock });
            return product;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //CartItems Repository

    public async findCartItemByProductId(id: string, quantity: number, stock: number): Promise<CartItemsEntity | boolean> {
        try {
            const cartItem: CartItemsEntity = await this.cartItemsRepository.findOneBy({ product: { id: id } });
            if (!cartItem) {
                return true;
            }
            cartItem.quantity += quantity;
            if (cartItem.quantity > +stock) {
                return false;
            }
            return true;
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }

    public async findCartItemOnlyByProductId(id: string): Promise<CartItemsEntity> {
        try {
            const cartItem: CartItemsEntity = await this.cartItemsRepository.findOneBy({ product: { id: id } });
            if (!cartItem) {
                throw new NotFoundException('No se encontro Producto.');
            }
            return cartItem;
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }

    public async createCartItems(body: CartItemsDTO[]): Promise<CartItemsEntity[]> {
        try {
            const cartItemsToUpdate: CartItemsBaseDTO[] = [];
            const cartItemsToSave: CartItemsDTO[] = [];
            for (const item of body) {
                const existingItem = await this.cartItemsRepository.findOneBy({ product: item.product });
                if (existingItem) {
                    existingItem.quantity += item.quantity;
                    cartItemsToUpdate.push(existingItem);
                } else {
                    cartItemsToSave.push(item);
                }
            }

            const newCartItems = await this.cartItemsRepository.save(cartItemsToSave);
            if(!newCartItems) {
                throw new BadRequestException('No se pudo crear los items de el pedido.');
            }

            for (const item of cartItemsToUpdate) {
                await this.cartItemsRepository.update({ id: item.id }, { quantity: item.quantity });
            }
            return [...newCartItems, ...cartItemsToUpdate];
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async deleteCartItemsByShoppingCartId(id: string): Promise<DeleteResult> {
        try {
            const cartItem: DeleteResult = await this.cartItemsRepository.delete({ shopping_session: { id: id } });
            if (cartItem.affected === 0) {
                throw new BadRequestException('No se pudo borrar.');
            }
            return cartItem;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findCartItemsByShoppingCartId(shoppingCartId: string): Promise<CartItemsEntity[]> {
        try {
            const cartItems: CartItemsEntity[] = await this.cartItemsRepository.find({
                where: { shopping_session: { id: shoppingCartId } },
                relations: ['product']
            });
            for (const cartItem of cartItems) {
                const product: ProductsEntity = await this.findProductById(cartItem.product.id);
                if (cartItem.quantity > +product.stock) {
                    throw new BadRequestException(`No hay stock del producto ${product.name} para la cantidad solicitada.`);
                }
            }
            if (cartItems.length === 0) {
                throw new BadRequestException('No se encontraron productos para este carrito de compras.');
            }
            return cartItems;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //Wallet Repository

    public async createWallet(body: WalletDTO): Promise<WalletEntity> {
        try {
            const newWallet = await this.walletRepository.create(body);
            if (!newWallet) {
                throw new BadRequestException('No se pudo crear la billetera.');
            }
            return newWallet;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async createWalletForUser(id: string): Promise<WalletEntity> {
        try {
            const user = await this.userRepository.findOneBy({ id: id });
            if (!user) {
                throw new BadRequestException('Se se encontro Usuario.');
            }
            const wallet: WalletDTO = { user: user };
            const newWallet = await this.walletRepository.save(wallet);
            if (!newWallet) {
                throw new BadRequestException('No se pudo crear la billetera.');
            }
            return newWallet;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async updateWalletBalance(id: string, balance: number): Promise<WalletEntity> {
        try {
            const wallet: WalletEntity = await this.walletRepository.findOneBy({ user: { id: id } });
            if (!wallet) {
                throw new NotFoundException('No se encontro Billetera para este usuario');
            }
            wallet.balance = balance;
            const updatedWallet = await this.walletRepository.save(wallet);
            if (!updatedWallet) {
                throw new BadRequestException('No se pudo crear la billetera.');
            }
            return updatedWallet;
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findWalletByUserId(id: string): Promise<WalletEntity> {
        try {
            const wallet: WalletEntity = await this.walletRepository
                .createQueryBuilder('wallet')
                .where({ user: id })
                .getOne();

            if (!wallet) {
                throw new NotFoundException('No se encontro Billetera para este usuario');
            }
            return wallet;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async updateWalletBalanceByUserId(id: string, addBalance: number): Promise<WalletEntity> {
        try {
            const wallet: WalletEntity = await this.walletRepository
                .createQueryBuilder('wallet')
                .where({ user: id })
                .getOne();

            if (!wallet) {
                throw new NotFoundException('No se encontro Billetera para este usuario');
            }

            wallet.balance += addBalance;
            return await this.walletRepository.save(wallet);
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //OrderItems Repository

    public async createOrderItems(body: OrderItemsDTO[]): Promise<OrderItemsEntity[]> {
        try {
            const newCartItems = await this.orderItemsRepository.save(body);
            if (!newCartItems) {
                throw new BadRequestException('No se pudo crear los items de el pedido.');
            }
            return newCartItems;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    //Util para extraer datos de Header

    public tokenData(header: any): any {
        const { jwt } = header;
        const token = userToken(jwt.toString());
        return token;
    }
}
