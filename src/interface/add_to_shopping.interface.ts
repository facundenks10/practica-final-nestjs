export interface IAddToShopping {
    products: IProductCart[];
}

export interface IProductCart {
    product_id: string;
    quantity: number;
}

export interface IShoppingSessionResponse {
    shopping_session_id: string;
}

export interface IDeleteCartItems{
    shoppingCartId: string;
    products: string[];
}

export class ICreateOrder{
    userId: string;
    order_id: string;
}