import { CartItemsEntity } from "src/modules/cart_items/entities/cart_items.entity";
import { OrderItemsEntity } from "src/modules/order_items/entities/order_items.entity";

export interface IProducts {
    name: string;
    price: number;
    stock: number;
    category: string;
    description: string;
    activated: boolean;
    cart_items: CartItemsEntity[];
    order_items: OrderItemsEntity[];
    quantityInOrders?: number;
    quantityInSCart?: number;
}


export interface FilterParams {
    name?: string;
    price?: number;
    category?: string;
    quantityInOrdersSort?: boolean;
    quantityInSCartSort?: boolean;
}

export interface FilterOrdersParams {
    orderStatus?: string;
    orderDate?: string;
}

export interface PaginationParams {
    currentPage: number;
    pageSize: number;
}

export interface PaginationResponse<IProducts> {
    currentPage: number;
    pageSize: number;
    data: IProducts[];
}

export interface PaginationOrdersResponse<FilterOrdersResponse> {
    currentPage: number;
    pageSize: number;
    data: FilterOrdersResponse[];
}

export interface UserDetails {
    id: string;
    lastName: string;
    firstName: string;
}

export interface OrderDetail {
    id: string;
    name: string;
    quantity: number;
    price: number;
    totalPrice: number;
}

export interface FilterOrdersResponse {
    user: UserDetails;
    price: number;
    paymentType: string;
    orderStatus: string;
    createdAt: Date;
    totalProducts: number;
    details: OrderDetail[];

}