export interface IUser {
    firstName: string;
    lastName: string;
    picture: string;
    email: string;
    address: string;
    password: string;
    activated: boolean;
    level_authority: string;
}