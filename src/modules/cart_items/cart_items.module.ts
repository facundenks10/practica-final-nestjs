import { Module } from '@nestjs/common';
import { CartItemsService } from './services/cart_items.service';
import { CartItemsController } from './controllers/cart_items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartItemsEntity } from './entities/cart_items.entity';
import { ShoppingSessionEntity } from '../shopping_session/entities/shopping_session.entity';
import { ProductsEntity } from '../products/entities/products.entity';
import { CommonModule } from 'src/common/common.momdule';


@Module({
  controllers: [CartItemsController],
  providers: [CartItemsService],
  imports: [TypeOrmModule.forFeature([CartItemsEntity,ShoppingSessionEntity, ProductsEntity]), CommonModule],
  exports: [CartItemsService, TypeOrmModule]
})
export class CartItemsModule {}
