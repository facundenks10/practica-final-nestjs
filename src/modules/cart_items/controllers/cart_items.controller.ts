import { Body, Controller, Delete, Get, Param, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { CartItemsService } from '../services/cart_items.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { DeleteCartItemsEntity } from '../entities/delete_cart_items';

@ApiTags('Cart-items')
@UseGuards(AuthGuard, RolesGuard)
@Controller('cart-items')
export class CartItemsController {
    constructor(
        private readonly cartItemsService: CartItemsService 
    ) { }
    

    @Roles('CLIENT')
    @Get(':id')
    public async findCartItemById(@Param('id') id: string) {
        return await this.cartItemsService.findCartItemById(id);
    }


    @Roles('CLIENT')
    @Delete('delete/:id')
    public async deleteCartItem(@Param('id') id: string) {
        return await this.cartItemsService.deleteCartItem(id);
    }

    @Roles('CLIENT')
    @Delete('delete_cart_items')
    public async deleteCartItems(@Body() body: DeleteCartItemsEntity) {
        return await this.cartItemsService.deleteCartItems(body);
    }    
}
