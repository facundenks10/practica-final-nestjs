import { ProductsEntity } from './../../products/entities/products.entity';
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsObject, IsOptional, IsUUID } from "class-validator";
import { BaseEntity } from 'src/config/base.entity';
import { ShoppingSessionEntity } from "src/modules/shopping_session/entities/shopping_session.entity";

export class CartItemsDTO{
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    quantity: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    shopping_session?: ShoppingSessionEntity;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    product?: ProductsEntity;
}

export class CartItemsBaseDTO extends BaseEntity{
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    quantity: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    shopping_session: ShoppingSessionEntity;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    product: ProductsEntity;
}

export class CartItemsUpdateDTO{
    @ApiProperty()
    @IsOptional()
    @IsNumber()
    quantity: number;

    @ApiProperty()
    @IsOptional()
    @IsUUID()
    shopping_session?: ShoppingSessionEntity;

    @ApiProperty()
    @IsOptional()
    @IsUUID()
    product?: ProductsEntity;
}