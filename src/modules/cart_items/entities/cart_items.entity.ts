import { BaseEntity } from "../../../config/base.entity";
import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { ICartItems } from "../../../interface/cart_items.interface";
import { ProductsEntity } from "../../../modules/products/entities/products.entity";
import { ShoppingSessionEntity } from "../../../modules/shopping_session/entities/shopping_session.entity";

@Entity({ name: 'cart_items' })
export class CartItemsEntity extends BaseEntity implements ICartItems {
    @Column()
    quantity: number;

    @ManyToOne(()=> ShoppingSessionEntity, (shopping_session) => shopping_session.cart_items)
    @JoinColumn({name: 'session_id'})
    shopping_session: ShoppingSessionEntity;

    @ManyToOne(()=> ProductsEntity, (product) => product.cart_items)
    @JoinColumn({name: 'product_id'})
    product: ProductsEntity;
}