import { IDeleteCartItems } from "src/interface/add_to_shopping.interface";

export class DeleteCartItemsEntity implements IDeleteCartItems{
    shoppingCartId: string;
    products: string[];
}