import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { CartItemsEntity } from '../entities/cart_items.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ErrorManager } from '../../../utils/error.manager';
import { CartItemsBaseDTO, CartItemsDTO } from '../dto/cart_items.dto';
import { DeleteResult, Repository } from 'typeorm';
import { DeleteCartItemsEntity } from '../entities/delete_cart_items';
import { IShoppingSessionResponse } from '../../../interface/add_to_shopping.interface';
import { ProductsEntity } from '../../../modules/products/entities/products.entity';
import { CommonService } from 'src/common/common.service';


@Injectable()
export class CartItemsService {
    constructor(
        @InjectRepository(CartItemsEntity)
        private readonly cartItemsRepository: Repository<CartItemsEntity>,
        private readonly commonService: CommonService
    ) { }

    public async createCartItems(body: CartItemsDTO[]): Promise<CartItemsEntity[]> {
        try {
            const cartItemsToUpdate: CartItemsBaseDTO[] = [];
            const cartItemsToSave: CartItemsDTO[] = [];
            for (const item of body) {
                const existingItem = await this.cartItemsRepository.findOneBy({ product: item.product });
                if (existingItem) {
                    existingItem.quantity += item.quantity;
                    cartItemsToUpdate.push(existingItem);
                } else {
                    cartItemsToSave.push(item);
                }
            }

            const newCartItems = await this.cartItemsRepository.save(cartItemsToSave);

            for (const item of cartItemsToUpdate) {
                await this.cartItemsRepository.update({ id: item.id }, { quantity: item.quantity });
            }
            return [...newCartItems, ...cartItemsToUpdate];
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async findCartItems(): Promise<CartItemsEntity[]> {
        try {
            const cartItems: CartItemsEntity[] = await this.cartItemsRepository.find();
            if (cartItems.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontraron productos'
                })
            }
            return cartItems;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async findCartItemsByShoppingCartId(shoppingCartId: string): Promise<CartItemsEntity[]> {
        try {
            const cartItems: CartItemsEntity[] = await this.cartItemsRepository.find({
                where: { shopping_session: { id: shoppingCartId } },
                relations: ['product']
            });
            for (const cartItem of cartItems) {
                const product: ProductsEntity = await this.commonService.findProductById(cartItem.product.id);
                if (cartItem.quantity > +product.stock) {
                    throw new BadRequestException(`No hay stock del producto ${product.name} para la cantidad solicitada.`);
                }
            }
            if (cartItems.length === 0) {
                throw new BadRequestException('No se encontraron productos para este carrito de compras.')
            }
            return cartItems;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findCartItemById(id: string): Promise<CartItemsEntity> {
        try {
            const cartItem: CartItemsEntity = await this.cartItemsRepository
                .createQueryBuilder('cart_item')
                .where({ id })
                .leftJoinAndSelect('cart_item.shopping_session', 'shopping_session')
                .leftJoinAndSelect('cart_item.product', 'product')
                .getOne();
            if (!cartItem) {
                throw new BadRequestException('No se producto.');
            }

            return cartItem;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findCartItemByProductId(id: string, quantity: number, stock: number): Promise<CartItemsEntity | boolean> {
        try {
            const cartItem: CartItemsEntity = await this.cartItemsRepository.findOneBy({ product: { id: id } });
            if (!cartItem) {
                return true;
            }
            cartItem.quantity += quantity;
            if (cartItem.quantity > +stock) {
                return false;
            }
            return true;
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }



    public async deleteCartItems(body: DeleteCartItemsEntity): Promise<IShoppingSessionResponse> {
        try {
            const shoppingSession = await this.commonService.findShoppingSessionById(body.shoppingCartId);
            if (shoppingSession) {
                for (const id of body.products) {
                    const cartItem: CartItemsEntity | null = await this.cartItemsRepository
                    .createQueryBuilder('cartItem')
                    .where({ id })
                    .leftJoinAndSelect('cartItem.shopping_session', 'shopping_session')
                    .getOne();
                    
                    if (!cartItem) {
                       throw new BadRequestException('No se encontro producto.');
                    }

                    if (shoppingSession.id !== cartItem.shopping_session.id) {
                        throw new BadRequestException('No se entoontro productoo en carrito de compras.');
                    }
    
                    await this.deleteCartItem(id);
                }
                return { shopping_session_id: shoppingSession.id };
            } else {
                throw new BadRequestException('No se encontro carrito de compras.');
            }
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }
    
    public async deleteCartItem(id: string): Promise<DeleteResult> {
        try {
            const cartItem: DeleteResult = await this.cartItemsRepository.delete({id});
            if (cartItem.affected === 0) {
                throw new NotFoundException('No se pudo borrar.');
            }
            
            return cartItem;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async deleteCartItemsByShoppingCartId(id: string): Promise<DeleteResult> {
        try {
            const cartItem: DeleteResult = await this.cartItemsRepository.delete({ shopping_session: { id: id } });
            if (cartItem.affected === 0) {
                throw new NotFoundException('No se pudo borrar.');
            }
            return cartItem;
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }
}
