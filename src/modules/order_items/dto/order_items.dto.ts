import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsObject, IsString } from "class-validator";
import { OrderEntity } from "src/modules/orders/entities/order.entity";
import { ProductsEntity } from "src/modules/products/entities/products.entity";

export class OrderItemsDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    price: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    quantity: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    product: ProductsEntity;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    order: OrderEntity;
}