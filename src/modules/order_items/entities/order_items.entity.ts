import { BaseEntity } from "../../../config/base.entity";
import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { ProductsEntity } from "../../../modules/products/entities/products.entity";
import { IOrderItems } from "../../../interface/order_items.interface";
import { OrderEntity } from "../../../modules/orders/entities/order.entity";

@Entity({ name: 'order_items' })
export class OrderItemsEntity extends BaseEntity implements IOrderItems {
    @Column()
    quantity: number;

    @Column()
    price: number;

    @ManyToOne(()=> ProductsEntity, (product) => product.cart_items)
    @JoinColumn({name: 'product_id'})
    product: ProductsEntity;

    @ManyToOne(()=> OrderEntity, (order) => order.order_items)
    @JoinColumn({name: 'order_id'})
    order: OrderEntity;
}