import { Module } from '@nestjs/common';
import { OrderItemsService } from './services/order_items.service';
import { OrderItemsController } from './controllers/order_items.controller';
import { OrderItemsEntity } from './entities/order_items.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  providers: [OrderItemsService],
  controllers: [OrderItemsController],
  imports: [TypeOrmModule.forFeature([OrderItemsEntity])],
  exports: [OrderItemsService, TypeOrmModule]
})
export class OrderItemsModule {}
