import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';
import { OrderItemsEntity } from '../entities/order_items.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrderItemsDTO } from '../dto/order_items.dto';
import { ErrorManager } from '../../../utils/error.manager';

@Injectable()
export class OrderItemsService {
    constructor(
        @InjectRepository(OrderItemsEntity)
        private readonly orderItemsRepository: Repository<OrderItemsEntity>
    ){}

    public async createOrderItems(body: OrderItemsDTO[]): Promise<OrderItemsEntity[]> {
        try {
            const newCartItems = await this.orderItemsRepository.save(body);
            if (!newCartItems) {
                throw new BadRequestException('No se pudo crear los items de el pedido.');
            }
            return newCartItems;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }
}
