import { Controller, Post, UseGuards, Headers, Request, Query, Get, Put, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { OrdersService } from '../services/orders.service';
import { FilterOrdersParams, FilterParams, PaginationParams } from 'src/interface/produsct.interface';

@ApiTags('Orders')
@UseGuards(AuthGuard, RolesGuard)
@Controller('orders')
export class OrdersController {
    constructor(
        private readonly orderService: OrdersService
    ) { }

    @Roles('CLIENT')
    @Post('create')
    public async createOrder(@Headers() auth: any, @Request() req: any, @Headers() headers: any) {
        const userId = req.idUser;
        return await this.orderService.createOrder(auth);
    }

    @Roles('CLIENT')
    @Get('all')
    public async filterOrders(
        @Headers() headers: any,
        @Query('orderStatus') orderStatus?: string,
        @Query('orderDate') orderDate?: string,
        @Query('currentPage') currentPage: number = 1,
        @Query('pageSize') pageSize: number = 10) {
        const filterParams: FilterOrdersParams = { orderStatus, orderDate };
        const paginationParams: PaginationParams = { currentPage, pageSize };
        return await this.orderService.findOrders(filterParams, paginationParams, headers);
    }

    @Roles('CLIENT')
    @Put('edit-approved/:id')
    public async updateOrderStatusApproved(@Headers() headers: any, @Param('id') id: string) {
        return await this.orderService.confirmOrder(id, headers);
    }

    @Roles('CLIENT')
    @Put('edit-cancel/:id')
    public async updateOrderStatusCancel(@Headers() headers: any, @Param('id') id: string) {
        return await this.orderService.cancelOrder(id, headers);
    }
}
