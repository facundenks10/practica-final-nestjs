import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString } from "class-validator";
import { ORDER_STATUS } from "src/constants/roles";
import { UsersEntity } from "src/modules/users/entities/users.entity";

export class OrderDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    payment_type: string;

    @ApiProperty()
    @IsOptional()
    @IsEnum(ORDER_STATUS)
    order_status?: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    total_price: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsObject()
    user: UsersEntity;
}