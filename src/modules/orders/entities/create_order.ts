import { ICreateOrder } from "src/interface/add_to_shopping.interface";

export class CreateOrderEntity implements ICreateOrder {
    order_id: string;
    userId: string;
}