import { BaseEntity } from "../../../config/base.entity";
import { ORDER_STATUS } from "../../../constants/roles";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { UsersEntity } from "../../../modules/users/entities/users.entity";
import { IOrder } from "../../../interface/order.interface";
import { TransactionEntity } from "../../../modules/transaction/entities/transaction.entity";
import { OrderItemsEntity } from "../../../modules/order_items/entities/order_items.entity";

@Entity({ name: 'order' })
export class OrderEntity extends BaseEntity implements IOrder {
    @Column()
    payment_type: string;
    
    @Column({type: 'enum', enum: ORDER_STATUS, default: ORDER_STATUS.APPROVED_PAYMENT})
    order_status?: string;
    
    @Column()
    total_price: number;

    @ManyToOne(()=> UsersEntity, (user) => user.orders)
    @JoinColumn({name: 'user_id'})
    user: UsersEntity;

    @OneToMany( () => TransactionEntity, (transaction)=> transaction.order)
    orderIncludes: TransactionEntity[];

    @OneToMany( () => OrderItemsEntity, (order_item)=> order_item.order)
    order_items: OrderItemsEntity[];
}