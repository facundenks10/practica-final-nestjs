import { Module } from '@nestjs/common';
import { OrdersService } from './services/orders.service';
import { OrdersController } from './controllers/orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderEntity } from './entities/order.entity';
import { WalletEntity } from '../wallet/entities/wallet.entity';
import { ProductsEntity } from '../products/entities/products.entity';
import { OrderItemsEntity } from '../order_items/entities/order_items.entity';
import { UsersEntity } from '../users/entities/users.entity';
import { ShoppingSessionEntity } from '../shopping_session/entities/shopping_session.entity';
import { CartItemsEntity } from '../cart_items/entities/cart_items.entity';
import { CommonModule } from 'src/common/common.momdule';

@Module({
  controllers: [OrdersController],
  providers: [OrdersService],
  imports: [TypeOrmModule.forFeature([OrderEntity, WalletEntity, ProductsEntity, OrderItemsEntity, UsersEntity, ShoppingSessionEntity, CartItemsEntity]), CommonModule],
  exports: [OrdersService, TypeOrmModule]
})
export class OrdersModule {}
