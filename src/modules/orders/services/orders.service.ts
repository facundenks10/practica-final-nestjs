import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderEntity } from '../entities/create_order';
import { OrderEntity } from '../entities/order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';
import { OrderDTO } from '../dto/order.dto';
import { ORDER_STATUS } from '../../../constants/roles';
import { OrderItemsDTO } from '../../order_items/dto/order_items.dto';
import { CommonService } from 'src/common/common.service';
import { FilterOrdersParams, FilterOrdersResponse, PaginationOrdersResponse, PaginationParams } from 'src/interface/produsct.interface';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(OrderEntity)
        private readonly orderRepository: Repository<OrderEntity>,
        private readonly commonService: CommonService
    ) { }

    public async createOrder(header: any): Promise<CreateOrderEntity> {
        try {
            const token = this.commonService.tokenData(header);
            const user = await this.commonService.findUserById(token.sub.toString());
            if (!user) {
                throw new NotFoundException('No se  encontro el usuario.');
            }
            const shoppingSession = await this.commonService.findShoppingSessionByUserId(user.id);
            if (!shoppingSession) {
                throw new NotFoundException('No se  encontro carrito de compras.');
            }
            const cartItems = await this.commonService.findCartItemsByShoppingCartId(shoppingSession.id);

            let totalPrice = 0;
            for (const cartItem of cartItems) {
                totalPrice += (cartItem.product.price * cartItem.quantity);
            }

            const order: OrderDTO = {
                payment_type: 'Billetera',
                total_price: totalPrice,
                user: user
            }

            const wallet = await this.commonService.findWalletByUserId(user.id);

            if (wallet.balance < order.total_price) {
                order.order_status = ORDER_STATUS.PENDING_PAYMENT;
            }

            const newOrder = await this.orderRepository.save(order);
            const orderItems: OrderItemsDTO[] = [];
            for (const cartItem of cartItems) {
                const product = await this.commonService.findProductById(cartItem.product.id);
                orderItems.push({
                    order: newOrder,
                    product: product,
                    quantity: cartItem.quantity,
                    price: cartItem.product.price
                })
            }

            await this.commonService.createOrderItems(orderItems);
            for (const orderItem of orderItems) {
                await this.commonService.updateStockProduct(orderItem.product.id, orderItem.quantity);
            }
            if (order.order_status === ORDER_STATUS.APPROVED_PAYMENT) {
                await this.commonService.updateWalletBalance(user.id, wallet.balance - order.total_price);
            }
            await this.commonService.deleteCartItemsByShoppingCartId(shoppingSession.id);
            await this.commonService.deleteShoppingSessionById(shoppingSession.id);

            return {
                userId: user.id,
                order_id: newOrder.id
            };
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }

    }

    public async findOrders(filterParams: FilterOrdersParams, paginationParams: PaginationParams, header: any):
        Promise<PaginationOrdersResponse<FilterOrdersResponse>> {
        try {
            const token = this.commonService.tokenData(header);
            const user = await this.commonService.findUserById(token.sub.toString());
            if (!user) {
                throw new NotFoundException('No se  encontro el usuario.');
            }
            const query: any = {};

            if (filterParams.orderStatus) query.order_status = filterParams.orderStatus;
            if (filterParams.orderDate) {
                const startDate = filterParams.orderDate + 'T00:00:00.000Z';
                const endDate = filterParams.orderDate + 'T23:59:59.999Z';

                query.createdAt = Between(startDate, endDate);
            }

            const currentPage = paginationParams.currentPage || 1;
            const pageSize = paginationParams.pageSize || 10;
            const skip = (currentPage - 1) * pageSize;
            if (Object.keys(query).length === 0) {
                const allOrders: OrderEntity[] = await this.orderRepository.find({
                    where: {
                        user: { id: user.id },
                        order_status: ORDER_STATUS.APPROVED_PAYMENT,
                    },
                    relations: ['order_items', 'order_items.product'],
                    skip,
                    take: paginationParams.pageSize
                });

                if (allOrders.length === 0) {
                    throw new BadRequestException('No se encontraron Pedidos.');
                }

                const formattedOrders: FilterOrdersResponse[] = allOrders.map(order => ({
                    user: {
                        id: user?.id,
                        lastName: user?.lastName,
                        firstName: user?.firstName,
                    },
                    price: order.total_price,
                    paymentType: order.payment_type,
                    orderStatus: order.order_status,
                    createdAt: order.createdAt,
                    totalProducts: order.order_items ? order.order_items.length : 0,
                    details: order.order_items ? order.order_items.map(detail => ({
                        id: detail.id,
                        name: detail.product?.name,
                        quantity: detail.quantity,
                        price: detail.price,
                        totalPrice: detail.price * detail.quantity,
                    })) : [],
                }));

                formattedOrders.forEach(order => {
                    order.totalProducts = order.details.reduce((total, detail) => total + detail.quantity, 0);
                });

                return {
                    currentPage,
                    pageSize,
                    data: formattedOrders,
                };
            }

            const allOrders: OrderEntity[] = await this.orderRepository.find({
                where: {
                    ...query,
                    user: { id: user.id },
                    order_status: ORDER_STATUS.APPROVED_PAYMENT
                },
                relations: ['order_items', 'order_items.product'],
                skip,
                take: paginationParams.pageSize
            });

            if (allOrders.length === 0) {
                throw new BadRequestException('No se encontraron Pedidos.');
            }

            const formattedOrders: FilterOrdersResponse[] = allOrders.map(order => ({
                user: {
                    id: user?.id,
                    lastName: user?.lastName,
                    firstName: user?.firstName,
                },
                price: order.total_price,
                paymentType: order.payment_type,
                orderStatus: order.order_status,
                createdAt: order.createdAt,
                totalProducts: order.order_items ? order.order_items.length : 0,
                details: order.order_items ? order.order_items.map(detail => ({
                    id: detail.id,
                    name: detail.product?.name,
                    quantity: detail.quantity,
                    price: detail.price,
                    totalPrice: detail.price * detail.quantity,
                })) : [],
            }));

            formattedOrders.forEach(order => {
                order.totalProducts = order.details.reduce((total, detail) => total + detail.quantity, 0);
            });

            return {
                currentPage,
                pageSize,
                data: formattedOrders,
            };
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async confirmOrder(orderId: string, header: any): Promise<OrderEntity> {
        try {
            const token = this.commonService.tokenData(header);
            const user = await this.commonService.findUserById(token.sub.toString());

            const order = await this.orderRepository.findOneBy({ id: orderId, user: { id: user.id } });
            if (!order) {
                throw new BadRequestException('No se encontro el Pedido.');
            }

            if (order.order_status !== ORDER_STATUS.APPROVED_PAYMENT) {
                throw new BadRequestException('No se puede confirmar pedido o ya fue confirmado.');
            }

            order.order_status = ORDER_STATUS.APPROVED;
           
            const orderConfirmed = await this.orderRepository.save(order);
            if( !orderConfirmed ) {
                throw new BadRequestException('No se pudo confirmar el pedido.');
            }
            return orderConfirmed;
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async cancelOrder(orderId: string, header: any): Promise<OrderEntity> {
        try {
            const token = this.commonService.tokenData(header);
            const user = await this.commonService.findUserById(token.sub.toString());

            const order = await this.orderRepository.findOne({
                where: { id: orderId, user: { id: user.id } },
                relations: ['user', 'order_items', 'order_items.product'],
            });
            if (!order) {
                throw new NotFoundException('No se encontro pedido.');
            }
            if (order.order_status !== ORDER_STATUS.APPROVED_PAYMENT) {
                throw new BadRequestException('No se puede cancelar pedido o ya fue cancelado.');
            }

            order.order_status = ORDER_STATUS.CANCELED_PAYMENT;
            await this.orderRepository.save(order);

            await this.commonService.updateWalletBalanceByUserId(user.id, order.total_price);

            for (const orderItem of order.order_items) {
                if (orderItem.product) {
                    await this.commonService.updateStockAddProduct(orderItem.product.id, orderItem.quantity);
                }
            }
            return order;
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }
}
