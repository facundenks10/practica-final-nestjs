import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { ProductsService } from '../services/products.service';
import { PublicAccess } from 'src/auth/decorators/public.decorator';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { ProductsDTO } from '../dto/products.dto';
import { FilterParams, PaginationParams } from 'src/interface/produsct.interface';
import { ErrorManager } from 'src/utils/error.manager';

@ApiTags('Products')
@UseGuards(AuthGuard, RolesGuard)
@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService) { }

    @Roles('ADMIN')
    @Post('register')
    public async registerProduct(@Body() body: ProductsDTO) {
        return await this.productsService.createProduct(body);
    }

    @Roles('CLIENT')
    @Get('all')
    public async findAllProjects(
        @Query('name') name?: string, 
        @Query('price') price?: number,
        @Query('category') category?: string,
        @Query('quantityInOrdersSort') quantityInOrdersSort: boolean = false,
        @Query('quantityInSCartSort') quantityInSCartSort: boolean = false,
        @Query('currentPage') currentPage: number = 1, 
        @Query('pageSize') pageSize: number = 10) {
            
        const filterParams: FilterParams = { name, price, category, quantityInOrdersSort, quantityInSCartSort };
        const paginationParams: PaginationParams = { currentPage, pageSize };
        
        return await this.productsService.findProducts(filterParams, paginationParams);
    }

    @Roles('ADMIN')
    @Get(':id')
    public async findUserById(@Param('id') id: string) {
        return await this.productsService.findProductById(id);
    }

    @Roles('ADMIN')
    @Put('update-stock')
    public async updateStock(@Query('id') id: string, @Query('stock') stock?: number) {
        return await this.productsService.updateStockProduct(id, stock);
    }
}
