import { BaseEntity } from "../../../config/base.entity";
import { Column, Entity, OneToMany } from "typeorm";
import { IProducts } from "../../../interface/produsct.interface";
import { CartItemsEntity } from "../../../modules/cart_items/entities/cart_items.entity";
import { OrderItemsEntity } from "../../../modules/order_items/entities/order_items.entity";

@Entity({ name: 'products' })
export class ProductsEntity extends BaseEntity implements IProducts {
    @Column()
    name: string;
    
    @Column()
    price: number;
    
    @Column()
    stock: number;
    
    @Column()
    category: string;
    
    @Column()
    description: string;

    @Column()
    activated: boolean;

    @OneToMany(() => CartItemsEntity, (cart_item) => cart_item.product)
    cart_items: CartItemsEntity[];

    @OneToMany(() => OrderItemsEntity, (order_item) => order_item.product)
    order_items: OrderItemsEntity[];
}