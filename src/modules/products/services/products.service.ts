import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { ProductsEntity } from '../entities/products.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductsDTO } from '../dto/products.dto';
import { ErrorManager } from '../../../utils/error.manager';
import { FilterParams, IProducts, PaginationParams, PaginationResponse } from '../../../interface/produsct.interface';

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(ProductsEntity)
        private readonly productRepository: Repository<ProductsEntity>,
    ) { }

    public async createProduct(body: ProductsDTO): Promise<ProductsEntity> {
        try {
            if (body.stock < 0) {
                throw new BadRequestException('El stock no puede ser negativo.');
            } else {
                return await this.productRepository.create(body);
            }

        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findProducts(filterParams: FilterParams, paginationParams: PaginationParams):
        Promise<PaginationResponse<IProducts>> {
        try {
            const query: any = {};

            if (filterParams.name) query.name = filterParams.name;
            if (filterParams.price) query.price = filterParams.price;
            if (filterParams.category) query.category = filterParams.category;

            const currentPage = paginationParams.currentPage || 1;
            const pageSize = paginationParams.pageSize || 10;

            const mapProductsToInterface = (product: ProductsEntity): IProducts => {
                const { name, price, stock, category, description, activated, order_items, cart_items } = product;
                const quantityInOrders = 0;
                const quantityInSCart = 0;

                return { name, price, stock, category, description, activated, quantityInOrders, quantityInSCart, order_items, cart_items, };
            };

            if (Object.keys(query).length === 0) {
                const allProducts: IProducts[] = (await this.productRepository.find({
                    where: {
                        activated: true
                    },
                    relations: ['cart_items', 'order_items'],
                    skip: (currentPage - 1) * pageSize,
                    take: paginationParams.pageSize
                })).map(mapProductsToInterface);

                if (allProducts.length === 0) {
                    throw new BadRequestException('No se encontraron productos.');
                }

                allProducts.forEach(product => {
                    product.quantityInSCart = product.cart_items.reduce((total, cart_items) => total + cart_items.quantity, 0);
                    product.quantityInOrders = product.order_items.reduce((total, order_items) => total + order_items.quantity, 0);
                });

                if (filterParams.quantityInOrdersSort) {
                    allProducts.sort((a, b) => b.quantityInOrders - a.quantityInOrders);
                }
                if (filterParams.quantityInSCartSort) {
                    allProducts.sort((a, b) => b.quantityInSCart - a.quantityInSCart);
                }

                const paginationResponse: PaginationResponse<IProducts> = {
                    currentPage,
                    pageSize,
                    data: allProducts
                };

                return paginationResponse;
            }

            query.activated = true;

            const skip = (currentPage - 1) * pageSize;

            const products: IProducts[] = (await this.productRepository.find({
                where: query,
                relations: ['cart_items', 'order_items'],
                skip,
                take: paginationParams.pageSize
            })).map(mapProductsToInterface);

            if (products.length === 0) {
                throw new BadRequestException('No se encontraron productos.');
            }

            products.forEach(product => {
                product.quantityInSCart = product.cart_items.reduce((total, cart_items) => total + cart_items.quantity, 0);
                product.quantityInOrders = product.order_items.reduce((total, order_items) => total + order_items.quantity, 0);
            });

            if (filterParams.quantityInOrdersSort) {
                products.sort((a, b) => b.quantityInOrders - a.quantityInOrders);
            }
            if (filterParams.quantityInSCartSort) {
                products.sort((a, b) => b.quantityInSCart - a.quantityInSCart);
            }

            const paginationResponse: PaginationResponse<IProducts> = {
                currentPage,
                pageSize,
                data: products
            };

            return paginationResponse;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findProductById(id: string): Promise<ProductsEntity> {
        try {
            const product: ProductsEntity = await this.productRepository
                .createQueryBuilder('product')
                .where({ id })
                .leftJoinAndSelect('product.cart_items', 'cart_item')
                .leftJoinAndSelect('product.order_items', 'order_item')
                .getOne();
            if (!product) {
                throw new NotFoundException('El producto no existe.');
            }
            return product;
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async updateStockProduct(id: string, stock: number): Promise<ProductsEntity> {
        try {
            const product = await this.productRepository.findOneBy({ id: id });
            if (!product || !product.activated) {
                throw new BadRequestException('No se encontro Productos.');
            }

            let newStock = product.stock - stock;

            if (newStock < 0) {
                throw new BadRequestException('No hay stock suficiente.');
            }

            product.stock = newStock;
            await this.productRepository.update({ id: id }, { stock: newStock });

            return product;
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }
}
