import { Body, Controller, Get, Param, Post, UseGuards, Headers, Delete } from '@nestjs/common';
import { ShoppingSessionService } from '../services/shopping_session.service';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { ShoppingSessionDTO } from '../dto/shopping_session.dto';
import { AddToShoppingEntity } from '../entities/add_to_shopping.entity';
import { userToken } from 'src/utils/user.token';

@ApiTags('Shopping-session')
@UseGuards(AuthGuard, RolesGuard)
@Controller('shopping-session')
export class ShoppingSessionController {
    constructor(
        private readonly shoppingSessionService: ShoppingSessionService, 
    ) { }

    @Roles('CLIENT')
    @Post('register')
    public async registerShoppingSession(@Body() body: ShoppingSessionDTO) {
        return await this.shoppingSessionService.createShoppingSession(body);
    }


    @Roles('CLIENT')
    @Post('register_cart_itemms')
    public async registerCartItemsByUserId(@Body() body: AddToShoppingEntity, @Headers() header: any) {
        return await this.shoppingSessionService.addToShoppingSession(body, header);
    }

    @Roles('CLIENT')
    @Get('all')
    public async findAllShoppingSessions() {
        return await this.shoppingSessionService.findShoppingSessions();
    }

    @Roles('CLIENT')
    @Get(':id')
    public async findShoppingSessionById(@Param('id') id: string) {
        return await this.shoppingSessionService.findShoppingSessionByUserId(id);
    }
}
