import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsNumber, IsOptional, IsUUID } from "class-validator"
import { CartItemsEntity } from "src/modules/cart_items/entities/cart_items.entity";
import { UsersEntity } from "src/modules/users/entities/users.entity"

export class ShoppingSessionDTO{
    @ApiProperty()
    @IsNotEmpty()
    @IsUUID()
    user: UsersEntity;

    @ApiProperty()
    @IsNotEmpty()
    @IsUUID()
    cart_items: CartItemsEntity[]
}

export class ShoppingSessionUpdateDTO{
    @ApiProperty()
    @IsOptional()
    @IsUUID()
    user: UsersEntity;

    @ApiProperty()
    @IsOptional()
    @IsUUID()
    cart_items: CartItemsEntity[]
}