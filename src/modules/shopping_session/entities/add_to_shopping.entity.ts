import { IAddToShopping, IProductCart } from "src/interface/add_to_shopping.interface";

export class AddToShoppingEntity implements IAddToShopping {
    products: IProductCart[];
}