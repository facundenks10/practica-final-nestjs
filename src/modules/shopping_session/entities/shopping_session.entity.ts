import { BaseEntity } from "../../../config/base.entity";
import { Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { UsersEntity } from "../../users/entities/users.entity";
import { CartItemsEntity } from "../../cart_items/entities/cart_items.entity";

@Entity({ name: 'shopping_session' })
export class ShoppingSessionEntity extends BaseEntity {
    @OneToOne(()=> UsersEntity)
    @JoinColumn()
    user: UsersEntity;

    @OneToMany( () => CartItemsEntity, (cart_items)=> cart_items.shopping_session)
    cart_items: CartItemsEntity[];
}