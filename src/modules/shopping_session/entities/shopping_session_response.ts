import { IShoppingSessionResponse } from "src/interface/add_to_shopping.interface";

export class ShoppingSessionResponseEntity implements IShoppingSessionResponse{
    shopping_session_id: string;
}