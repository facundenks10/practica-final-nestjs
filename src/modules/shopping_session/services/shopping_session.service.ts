import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';
import { ShoppingSessionEntity } from '../entities/shopping_session.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ErrorManager } from '../../../utils/error.manager';
import { ShoppingSessionDTO } from '../dto/shopping_session.dto';
import { AddToShoppingEntity } from '../entities/add_to_shopping.entity';
import { CartItemsDTO } from '../../../modules/cart_items/dto/cart_items.dto';
import { ShoppingSessionResponseEntity } from '../entities/shopping_session_response';
import { CommonService } from 'src/common/common.service';
import { IProductCart } from 'src/interface/add_to_shopping.interface';

@Injectable()
export class ShoppingSessionService {
    constructor(
        @InjectRepository(ShoppingSessionEntity)
        private readonly shoppingSessionRepository: Repository<ShoppingSessionEntity>,
        private readonly commonService: CommonService
    ) { }

    public async createShoppingSession(body: ShoppingSessionDTO): Promise<ShoppingSessionEntity> {
        try {
            return await this.shoppingSessionRepository.save(body);
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }

    public async addToShoppingSession(body: AddToShoppingEntity, header: any): Promise<ShoppingSessionResponseEntity> {
        try {

            const token = this.commonService.tokenData(header);
            const user = await this.commonService.findUserById(token.sub.toString());
            if(!user){
                throw new BadRequestException('El usuario no existe');
            }
            let shoppingSession: ShoppingSessionEntity = await this.shoppingSessionRepository.findOneBy({ user: { id: user.id } });  

            const productsMap = new Map<string, number>();

            for (const product of body.products) {

                if (productsMap.has(product.product_id)) {
                    productsMap.set(product.product_id, productsMap.get(product.product_id)! + +product.quantity);
                } else {
                    productsMap.set(product.product_id, +product.quantity);
                }
                const productSelected = await this.commonService.findProductById(product.product_id);
                if(!productSelected){
                    throw new BadRequestException('El producto con id: ' + product.product_id + ' no existe');
                }
                const cartItem = await this.commonService.findCartItemByProductId(product.product_id, product.quantity, productSelected.stock);

                if (!cartItem) {
                    throw new ErrorManager({
                        type: 'BAD_REQUEST',
                        message: `El stock del producto ${productSelected.name} es ${productSelected.stock} y la cantidad que quiere agregar supera el stock`
                    });
                }
            }

            body.products = Array.from(productsMap.entries()).map(([product_id, quantity]) => ({ product_id, quantity }));

            if (shoppingSession) {
                const cartItems = this.createCartItems(body.products, shoppingSession);
                this.commonService.createCartItems(cartItems);
            } else {
                const shoppingSessionEntity = await this.createShoppingSession({ user: user, cart_items: [] });
                const cartItems = this.createCartItems(body.products, shoppingSessionEntity);
                this.commonService.createCartItems(cartItems);
                shoppingSession = shoppingSessionEntity;
            }
            return { shopping_session_id: shoppingSession.id };
        } catch (error) {
            if (error instanceof BadRequestException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public createCartItems = (products: IProductCart[], shoppingSession: ShoppingSessionEntity) => {
        const cartItems: CartItemsDTO[] = products.map(product => {
            return {
                product: Object({ id: product.product_id }),
                quantity: product.quantity,
                shopping_session: shoppingSession,
            };
        });
        return cartItems;
    };

    public async findShoppingSessions(): Promise<ShoppingSessionEntity[]> {
        try {
            const shoppingSession: ShoppingSessionEntity[] = await this.shoppingSessionRepository.find();
            if (shoppingSession.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontraron carritos de compras'
                })
            }
            return shoppingSession;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async findShoppingSessionByUserId(id: string): Promise<ShoppingSessionEntity> {
        try {
            const shoppingSession: ShoppingSessionEntity = await this.shoppingSessionRepository
                .createQueryBuilder('user')
                .where({ id })
                .getOne();
            if (!shoppingSession) {
                throw new BadRequestException('El carrito de compras no existe');   
            }
            return shoppingSession;
        } catch (error) {
            throw new ConflictException('Hubo un error durante el proceso.');
        }
    }
}


