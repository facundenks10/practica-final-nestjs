import { Global, Module } from '@nestjs/common';
import { ShoppingSessionController } from './controllers/shopping_session.controller';
import { ShoppingSessionService } from './services/shopping_session.service';
import { ShoppingSessionEntity } from './entities/shopping_session.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartItemsEntity } from '../cart_items/entities/cart_items.entity';
import { UsersEntity } from '../users/entities/users.entity';
import { ProductsEntity } from '../products/entities/products.entity';
import { CommonModule } from '../../common/common.momdule';

@Module({
  controllers: [ShoppingSessionController],
  providers: [ShoppingSessionService],
  imports: [TypeOrmModule.forFeature([ShoppingSessionEntity, CartItemsEntity, UsersEntity, ProductsEntity]), CommonModule],
  exports: [ShoppingSessionService, TypeOrmModule]
})
export class ShoppingSessionModule {}
