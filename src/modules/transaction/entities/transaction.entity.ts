import { ITransaction } from "../../../interface/transaction.interface";
import { BaseEntity } from "../../../config/base.entity";
import { ORDER_STATUS } from "../../../constants/roles";
import { Column, Entity, ManyToOne } from "typeorm";
import { UsersEntity } from "../../../modules/users/entities/users.entity";
import { OrderEntity } from "../../../modules/orders/entities/order.entity";

@Entity({ name: 'transaction' })
export class TransactionEntity extends BaseEntity implements ITransaction {
    @Column({type: 'enum', enum: ORDER_STATUS})
    order_status: string;
    
    @Column()
    total_price: number;

    @ManyToOne( () => UsersEntity, (user)=> user.transactionIncludes)
    user: UsersEntity;
    
    @ManyToOne(() => OrderEntity, (order)=> order.orderIncludes)
    order: OrderEntity;
}