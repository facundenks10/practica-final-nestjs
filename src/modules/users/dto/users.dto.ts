import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsEnum, IsNotEmpty, IsObject, IsOptional, IsString } from "class-validator";
import { ROLES } from "src/constants/roles";
import { WalletEntity } from "src/modules/wallet/entities/wallet.entity";

export class UserDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    firstName: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    lastName: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    picture: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    address: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    password: string;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    activated: boolean;

    @ApiProperty()
    @IsOptional()
    @IsEnum(ROLES)
    level_authority: ROLES;

    @ApiProperty()
    @IsOptional()
    @IsObject()
    wallet?: WalletEntity;
}

export class UserUpdateDTO {
    @ApiProperty()
    @IsOptional()
    @IsString()
    firstName: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    lastName: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    picture: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    email: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    address: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    password: string;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    activated: boolean;

    @ApiProperty()
    @IsOptional()
    @IsEnum(ROLES)
    level_authority: ROLES = ROLES.ADMIN;
}