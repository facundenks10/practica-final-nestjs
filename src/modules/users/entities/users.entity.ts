import { TransactionEntity } from "../../../modules/transaction/entities/transaction.entity";
import { BaseEntity } from "../../../config/base.entity";
import { ROLES } from "../../../constants/roles";
import { IUser } from "../../../interface/user.interface";
import { WalletEntity } from "../../../modules/wallet/entities/wallet.entity";
import { Column, Entity, OneToMany, OneToOne } from "typeorm";
import { OrderEntity } from "../../../modules/orders/entities/order.entity";
import { ShoppingSessionEntity } from "../../../modules/shopping_session/entities/shopping_session.entity";
import { Exclude } from "class-transformer";

@Entity({ name: 'users' })
export class UsersEntity extends BaseEntity implements IUser {
    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    picture: string;

    @Column({ unique: true })
    email: string;

    @Exclude()
    @Column()
    password: string;

    @Column()
    address: string;

    @Column({default: true})
    activated: boolean;

    @Column({type: 'enum', enum: ROLES, default: ROLES.CLIENT})
    level_authority: ROLES;

    @OneToOne(() => WalletEntity, (wallet) => wallet.user)
    wallet: WalletEntity;

    @OneToOne(() => ShoppingSessionEntity, (shopping_session) => shopping_session.user)
    shopping_session: ShoppingSessionEntity;

    @OneToMany(() => TransactionEntity, (transaction) => transaction.user)
    transactionIncludes: TransactionEntity[];

    @OneToMany(() => OrderEntity, (order) => order.user)
    orders: OrderEntity[];

    
}