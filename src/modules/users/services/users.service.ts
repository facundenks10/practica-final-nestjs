import { BadRequestException, ConflictException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserDTO, UserUpdateDTO } from "../dto/users.dto";
import * as bcrypt from 'bcrypt';
import { ErrorManager } from "../../../utils/error.manager";
import { UsersEntity } from "../entities/users.entity";
import { DeleteResult, Repository, UpdateResult } from "typeorm";
import { CommonService } from "src/common/common.service";

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UsersEntity)
        private readonly userRepository: Repository<UsersEntity>,
        private readonly commonService: CommonService
    ) { }

    public async createUser(body: UserDTO): Promise<UsersEntity> {
        try {
            const existingUser = await this.userRepository.findOne({ where: { email: body.email } });
            if (existingUser) {
                throw new BadRequestException('El usuario ya existe en el sistema.');
            }

            if (!body.email || !body.password) {
                throw new BadRequestException('El email y la contraseña son obligatorios.');
            }

            body.password = await bcrypt.hash(body.password, +process.env.HASH_SALT);
            const user = await this.userRepository.save(body);
            if (user) {
                user.wallet = await this.commonService.createWalletForUser(user.id);
            }
            return user;
        } catch (error) {
            if (error instanceof BadRequestException || error instanceof NotFoundException) {
                throw error;
            } else {
                throw new ConflictException('Hubo un error durante el proceso.');
            }
        }
    }

    public async findUsers(): Promise<UsersEntity[]> {
        try {
            const users: UsersEntity[] = await this.userRepository.find();
            if (users.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontraron usuarios'
                })
            }
            return users;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async findUserById(id: string): Promise<UsersEntity> {
        try {
            const user: UsersEntity = await this.userRepository
                .createQueryBuilder('user')
                .where({ id })
                .leftJoinAndSelect('user.wallet', 'wallet')
                .leftJoinAndSelect('user.orders', 'order')
                .leftJoinAndSelect('user.transactionIncludes', 'transaction')
                .leftJoinAndSelect('user.shopping_session', 'shopping_session')
                .getOne();
            if (!user) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro usuario'
                })
            }
            return user;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async findBy({ key, value }: { key: keyof UserDTO; value: any }) {
        try {
            const user: UsersEntity = await this.userRepository
                .createQueryBuilder('user')
                .addSelect('user.password')
                .where({ [key]: value })
                .getOne();

            return user;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async updateUser(body: UserUpdateDTO, id: string): Promise<UpdateResult> {
        try {
            const user: UpdateResult = await this.userRepository.update({ id }, body);
            if (user.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se puedo actualizar'
                })
            }
            return user;
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }

    public async deleteUser(id: string): Promise<DeleteResult> {
        try {
            const user: DeleteResult = await this.userRepository.delete({ id });
            if (user.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se puedo borrar'
                })
            } else {
                return user;
            }
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message);
        }
    }
}
