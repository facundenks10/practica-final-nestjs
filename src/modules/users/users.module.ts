import { Global, Module } from '@nestjs/common';
import { UsersController } from './controllers/users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersEntity } from './entities/users.entity';
import { UsersService } from './services/users.service';
import { WalletEntity } from '../wallet/entities/wallet.entity';
import { CommonModule } from 'src/common/common.momdule';

@Global()
@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [TypeOrmModule.forFeature([UsersEntity, WalletEntity]), CommonModule],
  exports: [UsersService, TypeOrmModule ] 
})
export class UsersModule {}
