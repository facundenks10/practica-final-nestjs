import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, IsUUID } from "class-validator";
import { BaseEntity } from "src/config/base.entity";
import { UsersEntity } from "src/modules/users/entities/users.entity";
import { Column } from "typeorm";

export class WalletDTO{
    @ApiProperty()
    @IsOptional()
    @IsString()
    balance?: number;

    @ApiProperty()
    @IsOptional()
    @IsString()
    money_type?: string;

    @ApiProperty()
    @IsOptional()
    @IsUUID()
    user?: UsersEntity;
}