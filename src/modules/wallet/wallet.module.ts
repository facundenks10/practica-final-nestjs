import { Module } from '@nestjs/common';
import { WalletController } from './controllers/wallet.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletEntity } from './entities/wallet.entity';
import { UsersService } from '../users/services/users.service';
import { WalletService } from './services/wallet.service';
import { UsersEntity } from '../users/entities/users.entity';

@Module({
  controllers: [WalletController],
  providers: [WalletService],
  imports: [TypeOrmModule.forFeature([WalletEntity])],
  exports: [WalletService, TypeOrmModule ]
})
export class WalletModule {}
