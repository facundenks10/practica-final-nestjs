import { AuthTokenResult, IUseToken } from "src/auth/interfaces/auth.interface";
import * as jwt from 'jsonwebtoken';

export const userToken = (token: string): IUseToken | string => {
    try {
        const decode = jwt.decode(token) as AuthTokenResult;
        const currentDate = new Date();
        const expiresDate = new Date(decode.exp);
        return {
            sub: decode.sub,
            firstName: decode.firstName,
            lastName: decode.lastName,
            picture: decode.picture,
            levelAuthority: decode.levelAuthority,
            isExpired: +expiresDate <= +currentDate/1000,
        }
    } catch (error) {
        return 'Token is invallid';
    }
}